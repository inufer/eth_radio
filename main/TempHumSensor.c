// ----------------------------------------------------------------------------
//I2C Configuration for SI7006-A20 Temperature and Humidity sensor
// ----------------------------------------------------------------------------
#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE
#include <stddef.h>
#include <string.h>
#include <stdio.h>
//#include <sys/time.h>
#include "TempHumSensor.h"
#include "gpio.h"
#include "interface.h"
#include "app_main.h"
#include "driver/gpio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <driver/i2c.h>
#include "addoncommon.h"
// ----------------------------------------------------------------------------

#define TAG "TempHumSensor"

#define I2C_MASTER_NUM I2C_NUM_0    /*!< I2C port number for master dev */
#define I2C_MASTER_TX_BUF_DISABLE 0 /*!< I2C master doesn't need buffer */
#define I2C_MASTER_RX_BUF_DISABLE 0 /*!< I2C master doesn't need buffer */
#define WRITE_BIT I2C_MASTER_WRITE  /*!< I2C master write */
#define READ_BIT I2C_MASTER_READ    /*!< I2C master read */
//#define I2C_MASTER_FREQ_HZ 90000    /*!< I2C master clock frequency*/
#define I2C_MASTER_FREQ_HZ 50000    /*!< I2C master clock frequency*/
#define ACK_CHECK_EN 0x1            /*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS 0x0           /*!< I2C master will not check ack from slave */
#define ACK_VAL 0x0                 /*!< I2C ack value */
#define NACK_VAL 0x1                /*!< I2C nack value */
#define Si7006_ADDR 0x40               // Si7006-A20 i2c address

bool initTempHum ()
{
    gpio_num_t scl;
    gpio_num_t sda;
    gpio_num_t rsti2c;	
    gpio_get_i2c(&scl,&sda,&rsti2c);
    ESP_LOGD(TAG,"I2C GPIO SDA: %d, SCL: %d",sda,scl);
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = sda;
    conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
    conf.scl_io_num = scl;
    conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
    conf.master.clk_speed = I2C_MASTER_FREQ_HZ;
    conf.clk_flags = 0;
    esp_err_t err = i2c_param_config(I2C_MASTER_NUM, &conf);
    ESP_LOGD(TAG,"I2C setup : %d\n",err);

    err = i2c_driver_install(I2C_MASTER_NUM, conf.mode, I2C_MASTER_RX_BUF_DISABLE, I2C_MASTER_TX_BUF_DISABLE, 0);
    if (err != ESP_OK)
    {
        ESP_LOGE(TAG,"I2C_driver_install failed %x",err);
        return false;
    }
    return true;
}

bool getTempHumid (bool check_if_new)
{
    //ESP_LOGD(TAG,"Read Temperature and Humidity");
    uint8_t dataI2C[2]={0};
	char buff_str[23];
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, Si7006_ADDR << 1 | I2C_MASTER_WRITE, ACK_CHECK_EN);
    i2c_master_write_byte(cmd, 0xF5, ACK_CHECK_EN);
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, Si7006_ADDR << 1 | I2C_MASTER_READ, ACK_CHECK_DIS);
    i2c_master_stop(cmd);
    esp_err_t res = i2c_master_cmd_begin(I2C_MASTER_NUM, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
    vTaskDelay(100);//wait 1ms for measurement in device
    cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, Si7006_ADDR << 1 | I2C_MASTER_READ, ACK_CHECK_EN);
    i2c_master_read(cmd, dataI2C, 2 , NACK_VAL);
    i2c_master_stop(cmd);
    res = i2c_master_cmd_begin(I2C_MASTER_NUM, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);

    if (res != ESP_OK)
    {
        ESP_LOGE(TAG,"I2C READ humidity error: %d",res);
        return false;
    }
    //else ESP_LOGD(TAG,"I2C READ humidity OK");
    float humidity = (((dataI2C[0] * 256 + dataI2C[1]) * 125.0) / 65536.0) - 6;
    //kprintf("I2C Humidity= %.2f  DT0=%d  DT1=%d\n",humidity,dataI2C[0],dataI2C[1]);

    cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, Si7006_ADDR << 1 | I2C_MASTER_WRITE, ACK_CHECK_EN);
    i2c_master_write_byte(cmd, 0xF3, ACK_CHECK_EN);
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, Si7006_ADDR << 1 | I2C_MASTER_READ, ACK_CHECK_DIS);
    i2c_master_stop(cmd);
    res = i2c_master_cmd_begin(I2C_MASTER_NUM, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
    vTaskDelay(100);//wait 1ms for measurement in device
    cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, Si7006_ADDR << 1 | I2C_MASTER_READ, ACK_CHECK_EN);
    i2c_master_read(cmd, dataI2C, 2 , NACK_VAL);
    i2c_master_stop(cmd);
    res = i2c_master_cmd_begin(I2C_MASTER_NUM, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);

    if (res != ESP_OK)
    {
        ESP_LOGE(TAG,"I2C READ Temperature error: %d",res);
        return false;
    }
    //else ESP_LOGD(TAG,"I2C READ Temperature OK");
    float cTemp = (((dataI2C[0] * 256 + dataI2C[1]) * 175.72) / 65536.0) - 46.85;
	
    sprintf(buff_str,"%.1f'C  %.1f%%RH",cTemp,humidity);
    //kprintf("I2C Temperature= %.2f  DT0=%d  DT1=%d\n",cTemp,dataI2C[0],dataI2C[1]);
	//ESP_LOGD(TAG,"I2C READ String:len=%d:str=%s",strlen(strTempHum),strTempHum);
	if ( strcmp(strTempHum,buff_str) == 0 && check_if_new) return false; //if the string remains the same 
	strcpy(strTempHum,buff_str);
	return true; //new String
}

void temp_hum()
{
        if (getTempHumid(false)) kprintf("##I2C Sensor: %s#\n",strTempHum);
    else kprintf("##I2C Sensor: The Sensor is not present\n");
}
